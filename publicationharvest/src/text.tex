%!TEX root = ../report.tex
\section{Einleitung}

Beim Harvesting von Publikationsdaten handelt es sich um sogenanntes \textit{fokussiertes crawling} (engl. \textit{focused crawling}), d.h. die gezielte Suche nach Inhalten, die bestimmte Eigenschaften erfüllen. Hierbei werden Webseiten gecrawlt und auf diese Eigenschaften untersucht. Solche Ressourcen, die die gewünschten Eigenschaften besitzen, werden geharvested. Das fokussierte Crawling beinhaltet selbst noch nicht das Extrahieren von Metadaten.

Die Extraktion von Metadaten ist wiederum ein eigenes Problem und gliedert sich in mehrere Teilprobleme, denn die Metadaten können an unterschiedlichen Stellen und in unterschiedlichen Formaten auftauchen. Zum einen sind Publikations-Metadaten in Publikationen selbst enthalten, zum Anderen sind sie in Referenzen auf Publikationen enthalten. Metadaten können also in unterschiedlichen Dokumenttypen und in unterschiedlichen Formaten vorkommen.

\Cref{fig:metareferenz,fig:metapdf,fig:metahtml,fig:metascholar} zeigen die Metadaten einer Publikation in Unterschiedlichen Formaten und Dokumenten.

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/metareferenz.png}
		\caption{\label{fig:metareferenz}Referenz auf eine Publikation in der Referenzliste der Homepage eines Autors.}
	\end{center}
\end{figure}

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/metapdf.png}
		\caption{\label{fig:metapdf}Metadaten im PDF Dokument der Publikation.}
	\end{center}
\end{figure}

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/metahtml.png}
		\caption{\label{fig:metahtml}Metadaten im HTML-Dokument der Publikation.}
	\end{center}
\end{figure}

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/metascholar.png}
		\caption{\label{fig:metascholar}Verschiedene Zitationsstile bei Google Scholar.}
	\end{center}
\end{figure}

Die Metadaten einer Veröffentlichung sind in dem veröffentlichten Dokument selbst in Form von Titel, Autor, etc vorhanden. Liegen die Publikationen selbst in Form von Dokumenten vor, ist es daher möglich, die Metadaten direkt aus dem Dokument zu extrahieren. Hierzu muss zuerst der Text aus der Publikation extrahiert werden. Der extrahierte Text kann dann auf Metadaten untersucht werden.

Oftmals sind wissenschaftliche Publikationen jedoch an unterschiedlichen Stellen - wie Verlagen, Portale der Fachzeitschriften oder Konferenzen - ver\-öffent\-licht und auf den Publikationsseiten einer Organisation (Hochschule) lediglich referenziert und nicht direkt zugänglich. In solchen Fällen hat man keinen Zugriff auf die Publikation selbst, sondern nur auf eine Referenz, die Metadaten wie Autor, Titel, Jahr, etc enthält, und wiederum in unterschiedlichen Formaten, auch genannt Stilen, vorkommen können. Um Metadaten aus den Referenzen zu gewinnen, müssen diese geparst werden. Dieses Problem bezeichnet man als \textit{citation parsing}. Hierbei muss der Stil der Referenz erkannt werden, um die einzelnen Felder zu extrahieren. Es ist also beschränkt einsetzbar auf bekannte Formate. Da Referenzen auf Webseiten in einer hohen Anzahl an unterschiedlichen Formaten auftauchen können, muss man sich auf gängige Formate beschränken. Referenzen, die diesen Formaten nicht folgen, werden somit nicht korrekt erkannt und geparst. Es ist aber natürlich möglich, Organisations-spezifische Formate zusätzlich zu integrieren, so fern diese bekannt sind.

Die extrahierten Metadaten können selbst wiederum in verschiedenen Formaten gespeichert werden. Gängige Bibliographie-Formate sind bspw Bibtex, RefMan, EndNote oder XML. Manchmal sind diese sogar in Literaturlisten bereits vorhanden oder verlinkt, allerdings kann man sich darauf nicht verlassen.
\Cref{lst:bibtex,lst:refman,lst:endnote,lst:freeciv} zeigen Beispiele verschiedener Bibliographie-Formate zur Beispielpublikation aus \Cref{fig:metareferenz}.

\begin{lstlisting}[label=lst:bibtex,caption=Bibliographieeintrag in Bibtex.]
@inproceedings{rentrop2012shadow,
	title={Shadow IT evaluation model},
	author={Rentrop, Christopher and Zimmermann, Stephan},
	booktitle={Computer Science and Information Systems (FedCSIS), 2012 Federated Conference on},
	pages={1023--1027},
	year={2012},
	organization={IEEE}
}
\end{lstlisting}

\begin{lstlisting}[label=lst:refman,caption=Bibliographieeintrag in RefMan.]
TY  - CONF
T1  - Shadow IT evaluation model
A1  - Rentrop, Christopher
A1  - Zimmermann, Stephan
JO  - Computer Science and Information Systems (FedCSIS), 2012 Federated Conference on
SP  - 1023
EP  - 1027
SN  - 1467307084
Y1  - 2012
PB  - IEEE
ER  - 
\end{lstlisting}

\begin{lstlisting}[label=lst:endnote,caption=Bibliographieeintrag in EndNote.]
%0 Conference Proceedings
%T Shadow IT evaluation model
%A Rentrop, Christopher
%A Zimmermann, Stephan
%B Computer Science and Information Systems (FedCSIS), 2012 Federated Conference on
%P 1023-1027
%@ 1467307084
%D 2012
%I IEEE
\end{lstlisting}

\begin{lstlisting}[label=lst:freeciv,caption=Bibliographieeintrag in FreeCite XML.]
<book ...>
	<rft:atitle>IT Evaluation Model</rft:atitle>
	<rft:spage>12</rft:spage>
	<rft:date>2012</rft:date>
	<rft:btitle>In: Proceedings of the Federated Conference on Computer Science and Information Systems (FedCSIS</rft:btitle>
	<rft:genre>proceeding</rft:genre>
	<rft:epage>1023</rft:epage>
	<rft:au>Rentrop Christopher</rft:au>
	<rft:au>Zimmermann Stephan Shadow</rft:au>
</book>
\end{lstlisting}

Das Extrahieren von Publikations-Metadaten in ein komplexes Problem, das mehrere Lösungsmöglichkeiten aus unterschiedlichen Bereichen der Informationsverarbeitung bietet und fordert. Im Folgen wird auf die einzelnen Lösungswege detailiert eingegangen.

\section{Übersicht}

Abstrahiert folgt die Extraktion von Metadaten immer folgendem Schema.

\begin{enumerate}
	\item[0.] Beschaffung relevanter Dokumente, bspw. HTML, PDF, PostScript.
	\item[1.] Extrahieren von Text aus einem Dokument (\textit{text extraction})
	\item[2.] Parsen des Textes und identifizieren von Kandidaten (\textit{document parsing})
	\item[3.] Extraktion der Metadaten aus den Kandidaten (\textit{text/citation parsing})
	
\end{enumerate}

Die Beschaffung von Dokumenten zur Extraktion von semi-strukturierten Publikations-Metadaten aus unstrukturierten Web-Daten kann an unterschiedlichen Stellen erfolgen.

\begin{enumerate}
	\item Live beim Harvesten der Webseiten. Hierbei wird jeder gefundene Link bzw jede geharvestete Seite direkt auf Publikationsdaten untersucht. Dies bezeichnet man als `'fokussiertes Crawling`'.
	\item Forensisch im Archiv. Dokumente werden aus den vom Harvester erstellten Archiven (warcs) extrahiert. Hierbei muüssen aus den Archiven die Webseiten und weitere relevante Dokumente wieder rekonstruiert werden.
	\item Forensisch mit Hilfe des Logs. Hierbei werden die Log-Daten des Harvesters analysiert. URLs auf relevante Dokumente werden extrahiert. Diese werden anschliessend erneut abgerufen und auf Publikationsdaten hin untersucht.
\end{enumerate}

Liegen die Dokumente vor, muss zunächst festgestellt werden, ob sie Publikationsdaten enthalten, d.h. es müssen Publikationsdaten bzw Referenzen identifiziert werden. Hierzu muss Text aus den Dokumenten extrahiert werden und dieser auf vorher definierte Eigenschaften untersucht werden. So gefundene Kandidaten können dann weiter analysiert und ggf. strukturiert werden. Dafür müssen die Kandidaten geparst werden. Das Parsing wird nicht für alle Kandidaten erfolgreich sein, daher erhält man als Ergebnis Listen von strukturierten und nicht strukturierten Publikationsdaten, die auf Webseiten gefunden wurden.

Die Suche in textbasierten Dokumenten wie HTML unterscheidet sich leicht zu der in binären Dokumenten wie PDF und PostScript. 

Unter der Annahme es liegen HTML-Seiten vor, findet die Extraktion der Metadaten in folgenden Prozessen statt.

\begin{enumerate}
	\item Identifizieren von Seiten / Dokumenten, die Publikationsdaten enthalten.
	\item Identifizieren von Referenzen auf den gefundenen Publikationsseiten.
	\item Extraktion der Metadaten durch \textit{citation parsing}.
\end{enumerate}

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/AP1}
		\caption{\label{fig:ap1}Extraktion von Metadaten.}
	\end{center}
\end{figure}


\subsection{Extrahieren von Dokumenten}

Grundlage zum Extrahieren von Publikationsdaten ist das Vorhandensein der Daten in Dokumenten mit definierten und spezifischen Formaten. Publikationen sowie Referenzen/Zitationen kommen vor allem in PDS, Postscript und HTML-Dokumenten vor, wobei binäre Formate hauptsächlich für Publikationen und HTML für Referenzlisten verwendet werden. Auch muss man unterscheiden zwischen den Referenzelisten eines Autors auf seiner Homepage, die eigene publizierte Werke referenziert, und der Referenzeliste in Publikationen, die zum großteil fremde Werke referenziert.

In diesem Arbeitspaket liegt der Schwerpunkt auf der Extraktion strukturierter Publikationsdaten aus unstrukturierten Webdaten. Im Fokus steht daher das Erkennen und Strukturieren von Zitationen / Referenzen in Publikationslisten auf Webseiten von Autoren. Lösungen zum extrahieren von Metadaten aus binären Publikationen werden später ausführlich diskutiert.

Grundsätzlich bieten sich drei Alternativen an, um an die HTML-Seiten kommen. Diese sind im \Cref{fig:htmlextract} beschrieben.

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/htmlextract}
		\caption{\label{fig:htmlextract}Extraktion von HTML-Seiten.}
	\end{center}
\end{figure}

Zum einen können die Seiten direkt beim Harvesten einer Domain an den Publikations-Extrakter weitergegeben werden. Der Vorteil ist, dass dadurch die Seiten direkt beim Harvesten auf Publikationsdaten hin untersucht werden können und keine weiteren Untersuchungen und keine weiteren Crawling-Läufe anfallen. Allerdings erfordert solch eine Lösung individuelle Anpassungen an eine Web-Harvesting-Software, was auch auf Grund der Notwendigkeit fortdauernder Anpassungen an neue Versionen unpraktikabel und teuer ist.

Eine weitere Möglichkeit ist daher das Extrahieren der HTML-Seiten aus den Archiven des Harvesters. Hierbei werden forensisch die Archive nach HTML-Seiten durchsucht und diese dann an den Publikations-Extraktor weitergegeben. Auch hierbei muss die gecrawlte Domain nicht erneut angesehen werden. Allerdings ist das Auspacken von und Suchen in Archiven ein nicht zu vernachlässigender Aufwand, der durchaus einiges an Rechenaufwand und Speicher benötigt, was in Komplexität abhängig von der Größe der Archive ist. Bei Archivierungssystemen, die viele Domains crawlen und archivieren und daher u.U. ohnehin unter Last und Platzmangel leiden, ist diese Lösung daher nicht wünschenswert.

Eine nächste Möglichkeit ergibt sich durch eine Analyse der Crawl-Log-Dateien des Harvesters. Hier befinden sich Informationen über gecrawlte URLs sowie deren Datentypen. Somit lässt sich recht einfach eine Liste von URLs erstellen, die auf HTML-Seiten verweisen. Über diese können die Seiten dann geziehlt abgerufen und analysiert werden. Diese Lösung ist am wenigsten komplex, erfordert aber ein zusätzliches abrufen von HTML-Seiten, was zu erhöhter Last im Netz und auf den Ziel-Servern führt. Da allerdings nur gezielt HTML-Seiten aufgerufen werden, ist der Aufwand um einiges geringer als ein vollständiger Crawl. Auch sind die Vorteile einer solchen Lösung bestechend. Die Implementierung ist denkbar einfach und fast vollkommen unabhängig von Software von Drittherstellern. Ausnahme ist hier das Format der Crawl Log-Dateien, das sich aber vorrausslichtlich nicht häufig ändert. Dadurch ist die Lösung flexibler und sowohl der Entwicklungs- als auch der Pflegeaufwand extrem gering im Vergleich zu den anderen beiden Lösungen.

\subsection{Extraktion von Publikationsdaten}

Aus den HTML-Seiten müssen Publikationsdaten extrahiert werden. Dieser Prozess gliedert sich in mehrere Schritte (\Cref{fig:pubextract}).

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/pubextract}
		\caption{\label{fig:pubextract}Extraktion von Publikationsdaten.}
	\end{center}
\end{figure}

\subsubsection{Identifizieren von Publikationsseiten.}

Seiten mit Publikationen enthalten i.d.R. deutliche Anzeichen, die auf das vorhandensein einer Publikationsliste deuten. Da Publikationen öffentlich - und damit auch mittels Suchmaschinen - gefunden werden sollen, reicht oft schon die Suche nach Schlüsselwörtern wie ``Publikation`` oder ``Veröffentlichung``. Da eine Suche im Quelltext einer Seite jedoch auch uninteressante Elemente wie Menu-Einträge, Links, oder auch Script-Snippets liefert, muss etwas genauer differenziert werden. Die Suche nach den Schlüsselwörtern sollte daher auf aussagekräftige Webseiten-Elemente wie Titel und Überschriften beschränkt werden.

\Cref{lst:pubident1,lst:pubident2} zeigen mögliche Implementierungen zur Identifikation von Publikationsseiten. Gesucht wird nach Titeln und Überschriften, die einen der Strings ``Veröffentlichung``, ``Publikation`` oder englisch ``Publication`` enthalten. Wird mindestens ein solches Element auf einer Seite gefunden, so wird diese als Publikationsseite betrachtet.

\begin{lstlisting}[label=lst:pubident1,caption=Identifikation von Publikationen in Python.]
def has_publications(html):
	pattern = compile("<(title|h).*>.*(Ver(ö|&ouml)ffentlichung|Publikation|Publication).*</(title|h).*>")
	return (findall(pattern,html)) > 0
\end{lstlisting}

\begin{lstlisting}[label=lst:pubident2,caption=Identifikation von Publikationen in Bash.]
egrep "<((t|T)itle|H|h)[[:digit:]]*>(Ver(ö|&ouml)ffentlichung|Publikation|Publication)" [html page]
\end{lstlisting}


So identifizerte Publikationsseiten werden in einem nächsten Schritt auf Referenz/Zitations-Listen hin untersucht.

\subsubsection{Identifizieren von Publikationskandidaten} \label{sec:pubident}

Auf gefundenen Publikations-Seiten müssen einzelne Referenzen identifiziert und strukturiert werden. Da die Publikationsliste praktisch beliebig auf der Seite gestaltet werden kann und somit in einer großen Anzahl unterschiedlicher Formate vorliegen kann, ist ein scharfes Abgrenzen von Publikationsdaten nahezu unmöglich. Diese enorme Vielfalt an verschiedenen Formaten bedingt daher eine Einschränkung auf gängige und leicht erkennbare Stile. Hierdurch ist ein Verlust der Genauigkeit möglich. 

Diesem Problem kann man unterschiedlich begegnen. Man kann Anwender dazu auffordern, Referenzen in gängigen Formaten zu veröffentlichen. Allerdings wird dies nur in wenigen Fällen erfolgreich sein. Alternativ können Kundenspezifische Regeln implementiert werden, die dort gängige Zitationsstile erfassen. Dies ist ein iterativer Prozess, da die Stile zu Beginn nicht unbedingt bekannt sind und diese erst durch Untersuchung der identifizierten Publikationsseiten auffallen.

Bibliographische Einträge sind oft eingebettet in eines der folgenden Elemente

\begin{itemize}
	\item Paragraphen: $<p>$ \textit{Zitation} $</p>$
	\item Listen: $<li>$ \textit{Zitation} $</li>$
	\item Tabellen: $<tr>$ \textit{Zitation} $</tr>$
\end{itemize}

Leider hat jedes dieser Elemente auch weitere, unterschiedliche Verwendungen. Paragraphen beinhalten auch anderen Text. Listen werden auch für Menus und Navigation verwedet. Publikationsdaten in Tabellen können beliebig komplex aufgebaut werden, z.b. gegliedert nach Autoren oder Jahren mit einzelnen Zeilen für unterschiedliche Angaben. Hier ist ein genaues Identifizieren ohne optische Analyse sehr schwierig.

Zusätzlich zu den Zitationen könnte man auch gezielt nach Links auf Dateien in gängigen Publikationsformaten (pdf, doc, bib) suchen. Der Link kann die Referenz selbst sein, oder ihr vor-/nachgestellt sein. Allerdings sind nicht alle Referenzen zwingend mit links ausgestattet und es ist nicht klar, ob, und in welchem Format bibliographische links vorhanden sind. Daher ist der Mehrwert eines solchen Vorgehens zweifelhaft, zumal Publikationen selbst gesondert eingesammelt werden können.

Um das Rauschen in den Publikationsdaten zu verringern ist es daher notwendig die Suche auf gängige Formate einzuschränken. Ein typischer bibliographischer Eintrag auf einer Webseite sieht bspw. wie folgt aus.

\vspace{4mm}
``Zimmermann, Stephan; Rentrop, Christopher: Schatten-IT; In: HMD – Praxis der Wirtschaftsinformatik 49 (2012), 288, S. 60-68.''
\vspace{4mm}

Die gleiche Referenz in von Google Scholar bereitgestellten Stilen:

\begin{enumerate}
	\item[MLA] Zimmermann, Stephan, and Christopher Rentrop. "Schatten-IT." HMD Praxis der Wirtschaftsinformatik 49.6 (2012): 60-68.
	
	\item[APA] Zimmermann, S., \& Rentrop, C. (2012). Schatten-IT. HMD Praxis der Wirtschaftsinformatik, 49(6), 60-68.
	
	\item[ISO690] ZIMMERMANN, Stephan; RENTROP, Christopher. Schatten-IT. HMD Praxis der Wirtschaftsinformatik, 2012, 49. Jg., Nr. 6, S. 60-68.
\end{enumerate}

Plugins für CMS wie Wordpress oder Typo3 strukturieren Publikationen oftmals in Tabellen. Der Text selbst folgt aber zumindest rudimentär gängigen Stilen.

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/CMSpub}
		\caption{\label{fig:CMSpub}Anzeige Publikationsdaten in Wordpress.}
	\end{center}
\end{figure}

Der verwendete Zitationsstil ist abhängig von mehreren Faktoren:

\begin{itemize}
	\item technische Vorgaben (bspw. eingesetztes CMS)
	\item organisatorische Vorgaben (bspw. Rechtlinien in der Organisation)
	\item Studienfeld, so sind in der Informatik andere Stile gängig wie z. bsp. in anderen Naturwissenschaften oder den Geisteswissenschaften.
	\item Persönliche Vorlieben des Autors. Dies wirkt meist schwerer als alle anderern Vorgaben. Jeder Autor hat eine eigene Vorstellung davon, wie seine Referenzliste aussehen soll.
\end{itemize}

Unabhängig vom verwendeten Stil lässt sich jedoch prinzipiell beobachten, dass eine Publikation mindestens einen Autor hat, einen Titel besitzt, und zumindest ein Publikationsjahr trägt. Auch sind bei gängigen Stilen die einzelnen Felder irgendwie voneinander getrennt, z.b. mit ',' oder ';'. Betrachtet man o.g. Zitationsstile, so lassen sich Regeln ableiten, denen eine Referenz folgen muss. Die in \Cref{lst:regex} gelisteten regulären Ausdrücke matchen jeweils die o.g. Zitationsstile. In Abhängigkeit von gefundenen Stilen auf identifizierten Publikationsseiten können diese relativ einfach um weitere Stile erweitert werden.

\begin{lstlisting}[label=lst:regex,caption=Reguläre Ausdrücke zum Matchen von Publikationsstilen.]
htwg = r'\w+,\s*\w+.*:\s*[a-zA-Z0-9_\s-]*;.*\(\d{4}\).*'
mla = r'\w+,\s*\w+.*."[a-zA-Z0-9_\s-]*\."\s*\w+.*\(\d{4}\).*'
apa = r'\w+,\s*\w\..*\(\d{4}\)\.\s*[a-zA-Z0-9_\s-]*\.\s*.*,.*'
iso = r'\w+,\s*\w+.*\.\s*[a-zA-Z0-9_\s-]*\..*,\s*\d{4},'
\end{lstlisting}

Der Code in \Cref{lst:iscitation} testet, ob ein Publikationskandidat eine Publikation gemäß den vorher definitieren Zitationsstilen ist.

\begin{lstlisting}[label=lst:iscitation,caption=Identifiziere erlaubte Zitationen.]
def isCitation(s):
	ret = False
	for style in citationstyles:
		ret |= (style.findall(s) != [])
		if ret == True:
			break
	return ret
\end{lstlisting}

Kandidaten, die als Zitation identifiziert sind, da sie einem bekanntem Stil entsprechen, werden im folgenden Schritt geparst und strukturiert.

\subsection{Strukturieren gefundener Einträge}

Das Strukturieren gefundener Einträge ist beliebig aufwändig. Aufgabe ist, die extrahierten Publikationskandidaten zu untersuchen und Meta-Informationen wie Autoren, Titel, Buch, Jahr, etc. automatisch zu identifizieren. Dies ist ein als \textit{citation parsing} bekanntes Problem. Einige Projekte und auch Web-Services versuchen bereits dieses Problem zu lösen. Allerdings beinhaltet \textit{citation parsing} auch das parsen bereits strukturierter bibliographischer Einträge und deren Darstellung im Speicher. Da dies u.U. enorme linguistische Analysen erfordert, und aufgrund der hohen Anzahl unterschiedlicher Formate beliebig Komplex sein kann, sollte auf eine eigene und proprietäre Implementierung verzichtet und auf bereits existierende Mechanismen zurückgegriffen werden.

Es existieren einige öffentliche Suchmaschinen, die bereits die Suche nach bibliographischen Einträgen erlauben. Solche Portale suchen entweder in eigenen Datenbanken (wie WorldCat oder auch Pubmed) oder sie durchsuchen auch andere Datenbanken (Meta-Suchmaschinen wie Google Scholar oder auch Microsoft Academic Search). Der Such-String wird von solchen Suchmaschinen verwendet, um die Datenbanken nach passenden Einträgen zu durchsuchen. Gefundene Einträge werden dem Nutzer zurück gegeben. Oftmals gibt es zusätzlich die Möglichkeit Ergebnisse in unterschiedliche bibliographische Formate (wie bibtex) zu exportieren, woraufhin man direkt strukturierte Einträge zur Weiterverarbeitung erhält. Es gibt mehrere öffentliche bibliographische Suchmaschinen, die sich in wichtigen Punkten wie Ergebnisqualität, Nutzungsbedingungen, Exportmöglichkeiten, Vorhandensein und Mächtigkeit einer API, unterscheiden. 

Der entscheidende Nachteil solcher bibliographischen Suchmaschinen ist, dass sie auf indexierte Einträge in bibliographischen Datenbanken angewiesen sind. Während diese Suchmaschinen sehr gute Ergebnisse produzieren können, sofern es passende Datenbankeinträge gibt, liefern sie leider gar kein Ergebnis für Suchen, die keine Entsprechung in Datenbanken haben. Sie dienen also nicht dazu, Publikationskandidaten zu strukturieren, die nicht bereits in einer Datenbank vorhanden sind. Auch ist man bei webbasierten Suchmaschinen an Nutzungsbedingen gebunden und dadurch in der Nutzung eingschränkt.

Mögliche bibliographische Suchmaschinen beinhalten die folgenden.

\begin{itemize}
	\item \href{http://scholar.google.de/}{Google Scholar}
	\item \href{http://academic.research.microsoft.com/}{Microsoft Academic Search}
	\item \href{https://www.ncbi.nlm.nih.gov/pubmed/}{Pubmed}
	\item \href{http://www.crossref.org/}{Crossref}
	\item \href{https://www.worldcat.org/}{WorldCat}
\end{itemize}

Neben den bibliographischen Suchmaschinen gibt es noch Software Projekte die auch ohne bibliographische Datenbanken versuchen Referenzen zu parsen und zu strukturieren. Hierbei gibt man einen Referenz-String an, dieser wird geparst, analysiert und strukturiert. Diese Projekte versuchen Meta-Informationen anhand der Referenz direkt zu extrahieren. Der Vorteil einer solchen Lösung ist, dass man auf keine Publikationsdatenbank angewiesen ist und somit auch Publikationskandidaten strukturiert werden können, die in keiner Datenbank indexiert sind. Da man sich das Suchen in Datenbanken erspart ist diese Lösung auch erheblich ressourcenschonender und um einiges schneller. Allerdings können die Ergebnisse u.U. ungenauer sein, da kein Abgleich mit bereits bekannten Einträgen erfolgt.

Quelloffene Projekte, die Meta-Daten-Extraktion aus Referenzen ermöglichen beinhalten die folgenden.

\begin{itemize}
	\item \href{http://freecite.library.brown.edu/}{FreeCite}
	\item \href{http://aye.comp.nus.edu.sg/parsCit/index.html}{ParsCit}
	\item \href{http://citeseerx.ist.psu.edu}{Citeseer}
\end{itemize}

Im folgenden werden die vorgestellten Portale und Software-Projekte auf ihre Anwendbarkeit untersucht.

\subsubsection{Google Scholar}

Leider bietet Google bisher keine API für Scholar an \cite{googlescholarapi}.
Es gibt aber Open Source Parser wie \href{https://github.com/ckreibich/scholar.py}{scholar.py}. Dieses bietet eine Reihe nützlicher Optionen mit denen nach Titeln, Autoren oder beliebigen Strings gesucht werden kann. Außerdem beherrscht es eine ganze Reihe verschiedener Ausgabeformate, u.a. Bibtex. \Cref{lst:scholarpy} zeigt ein Beispiel anhand des in \Cref{sec:pubident} gefundenen Publikationseintrags.

\begin{lstlisting}[label=lst:scholarpy,caption=Parsen und Konvertieren von Zitationen mit scholar.py.]
python scholar.py -A "Zimmermann, Stephan; Rentrop, Christopher: Schatten-IT; In: HMD - Praxis der Wirtschaftsinformatik 49 (2012), 288, S. 60-68." --citation "bt"

@article{zimmermann2012schatten,
	title={Schatten-IT},
	author={Zimmermann, Stephan and Rentrop, Christopher},
	journal={HMD Praxis der Wirtschaftsinformatik},
	volume={49},
	number={6},
	pages={60--68},
	year={2012},
	publisher={Springer}
}

@article{wiener2013hmd,
	title={HMD Best Paper Award 2012},
	author={Wiener, Martin and Denk, Reinhard},
	journal={HMD Praxis der Wirtschaftsinformatik},
	volume={50},
	number={2},
	pages={110--113},
	year={2013},
	publisher={Springer}
}
\end{lstlisting}

Die `'Terms of Use`' schließen die Verwendung von Robots aus und Google scholar setzt nach einer bisher noch nicht genau identifizerter Anzahl an Anfragen captchas zum Schutz vor Robots ein, was die Anwendbarkeit drastisch reduziert. Es ist daher notwendig, Anfragen an Google weniger aggresiv vorzunehmen, d.h. mit reduzierter Frequenz, um einen längerfristigen Ausschluss zu vermeiden. Leider gibt es kein Nutzungsmodell seitens Google, das einen Einsatz im Projekt erlauben würde, auch das Fehlen einer geeigneten API spricht dagegen.

\subsubsection{Microsoft Academic Search (MAS)}
bietet eine API \cite{masapi}
mit SOAP und JSON an. Um diese zu Verwenden muss man sich jedoch persönlich per E-Mail registrieren und die `'Terms of Use`' akzeptieren.

Rechtlich spricht nichts gegen die Nutzung von MAS. Allerdings schreiben auch hier die `'Terms of Use`' eine ``verantwortungsvolle Nutzung'' vor und Microsoft behält sich das Recht vor ohne Angabe von Gründen die Nutzung einzuschränken, was wieder auf das Vorhandensein eines Robot-Schutzes hindeutet.

Bei Tests hat sich ergeben, dass die Such-Ergebnisse auf MAS deutlich schlechter sind als bei Google Scholar. Die Suche nach o.g. Referenz liefert bspw. kein Ergebnis.

\subsubsection{PubMed}
ist eine Publikationsdatenbank mit dem Fokus auf Medizinische und Life Science Veröffentlichungen. Auch PubMed bietet eine OAI-PMH Schnittstelle \cite{pubmedoai}.
Da PubMed sehr spezialisiert ist, und im Vorfeld nicht automatisiert herausgefunden werden kann, in welchem Fachbereich eine Veröffentlichung angesiedelt ist, ist die Nutzung von PubMed nur bedingt sinnvoll. Wenn allerdings im Vorfeld die Art der Publikationen festehen, bspw. weil es sich um Publikationen einer Klinik o.ä. handelt, so lohnt sich PubMed evtl. mehr, als andere nicht spezialisierte Datenbanken.

\subsubsection{CrossRef}

CrossRef ist eine Suchmaschine speziell für Publikations-Metadaten. Daher bietet sich der Einsatz hier an. CrossRef hat sich spezialisiert auf das Indexieren von \textit{Document Object Identifiers}, also persistenter Identifikationsnummber von Dokumenten. CrossRefs Datenbank wird direkt gespeist durch teilnehmende Organisationen. Auch CrossRef bietet den Export strukturierter bibliographischer Daten in verschiedenen Formaten wie bibtex oder RIS an.

CrossRef bietet eine einfache API mittels formatierter GET-Requests an.

\begin{lstlisting}[label=lst:crossrefget,caption=GET requests an CrossRef.]
url = "http://search.crossref.org/?q=" + string
html = urlopen(url).read()
\end{lstlisting}

Auch gibt es open source Projekte, wie \href{https://github.com/MartinPaulEve/crossRefQuery/}{crossRefQuery} die bereits das Exportieren formatierter bibliographischer Einträge mittels einer Suche auf CrossRef implementieren. Eine Suche bei CrossRef nach obiger Referenz liefert das folgende Ergebnis:

\begin{lstlisting}[label=lst:crossrefresult,caption=Beispielergebnis einer CrossRef-Suche.]
@article{Zimmermann_2012,
title={Schatten-IT},
volume={49},
ISSN={2198-2775},
url={http://dx.doi.org/10.1007/bf03340758},
DOI={10.1007/bf03340758},
number={6},
journal={HMD},
publisher={Springer Fachmedien Wiesbaden GmbH},
author={Zimmermann, Stephan and Rentrop, Christopher},
year={2012},
month={Dec},
pages={60--68}
}
\end{lstlisting}

Crossref kann zuverlässig Referenzen finden, die auf Publikationen zeigen, die bei teilnehmenden Partnern veröffentlicht wurden. Während eine Webbasierte Suche den Export von bibliographischen Einträgen bietet, erlaubt die API leider nur einfache automatisierte Suchen nach DOIs. Es ist daher nicht unerheblich aufwändig mittels Crossref strukturierte Metadaten zu gewinnen.

\subsubsection{WorldCat}
ist nach eigenen Angaben das größte Netzwerk von Bibliothek-Inhalten. Als solches ist es allerdings spezialisiert auf die Suche nach Beständen in Bibliotheken, d.h. Bücher und Audio/Video Medien. Auch lässt sich gezielt in bestimmten Bibliotheken suchen. WorldCat bietet eine Such-API an. Zur Suche bibliographischer Einträge wissenschaftlicher Veröffentlichungen eignet sich WorldCat auf Grund der Spezifizierung auf Bibliotheken nicht. So liefert eine Suche nach o.g. Referenz kein Ergebnis.

\begin{lstlisting}[label=lst:worldcat,caption=Ergebnis einer Suche bei WorldCat.]
No results match your search for 'kw:Zimmermann, Stephan; Rentrop, Christopher: Schatten-IT; In: HMD - Praxis der Wirtschaftsinformatik 49 (2012), 288, S. 60-68.'. 
\end{lstlisting}

\subsubsection{ParsCit}

ParsCit ist ein quelloffenes Projekt der 'National University of Singapore', welches zwei Aufgaben erfüllt. 1) Parsen von Referenzen (\textit{citation parsing} und 2) Parsen von wissenschaftlichen Dokumenten / Veröffentlichungen. Der Quellcode von ParsCit kann kostenfrei heruntergeladen und verwendet werden. Es ist aber auch möglich das Webportal oder einen Web Service zu verwenden. Hierfür stellen die Autoren bereits Perl Code und Beispiele bereit, die sehr einfach in bestehende Anwendungen integriert werden können.

ParsCit erkennt eine Vielzahl von Referenzstilen und liefert relativ gute Ergebnisse, die in den Formaten bibtex oder XML ausgegeben werden können.

\begin{lstlisting}[label=lst:parscit,caption=Ausgabeformate von ParsCit.]
@InCollection{d1e5,
author="Zimmermann, Stephan",
title="Rentrop, Christopher: Schatten-IT; In:",
booktitle="HMD -- Praxis der Wirtschaftsinformatik",
year="2012",
pages="288--60"
}

<citation>
<authors>
<author>Stephan Zimmermann</author>
</authors>
<booktitle>HMD - Praxis der Wirtschaftsinformatik</booktitle>
<volume>49</volume>
<date>2012</date>
<title>Rentrop, Christopher: Schatten-IT; In:</title>
<pages>288--60</pages>
</citation>
\end{lstlisting}

ParsCit wird auch verwendet von CiteSeerX (siehe unten), um Referenzen und wissenschaftliche Dokumente zu parsen.

\subsubsection{FreeCite}
ist - ähnlich zu ParsCit und von diesem inspiriert - eine quelloffene Web Applikation, spezialisiert sich jedoch ausschließlich auf das Parsen von Referenzen. Wie ParsCit auch, kann der Quellcode heruntergeldaen und die Applikation selbst gehostet werden, oder die Web Applikation sowie ein Web Service der Autoren verwendet werden. Die API von FreeCite ist sehr einfach, selbst mit gängigen Kommandozeilentools verwendbar und leifert als Ergebnis strukturierte XML Dokumente. Die Ergebnisse selbst sind teilweise besser als bei ParsCit, wie folgendes Beispiel zeigt.

\begin{lstlisting}[label=lst:freecite,caption=Ergebnis einer Suche mit FreeCite.]
curl -H 'Accept: text/xml' -d "citation=Zimmermann, Stephan; Rentrop, Christopher: Schatten-IT; In: HMD - Praxis der Wirtschaftsinformatik 49 (2012), 288, S. 60-68." http://freecite.library.brown.edu/citations/create

<citations>
	<citation valid='true'>
		<authors>
			<author>Zimmermann Stephan</author>
			<author>Rentrop Christopher</author>
		</authors>
		<title>Schatten-IT; In: HMD - Praxis der Wirtschaftsinformatik 49</title>
		<pages>60--68</pages>
		<year>2012</year>
		<raw_string>Zimmermann, Stephan; Rentrop, Christopher: Schatten-IT; In: HMD - Praxis der Wirtschaftsinformatik 49 (2012), 288, S. 60-68.</raw_string>
	</citation>
</citations>
\end{lstlisting}

\subsubsection{CiteSeerX}
ist eine aktiv entwickelte digitale Bibliothek und Suchmaschine, die sich auf Veröffentlichungen in den Bereichen Informatik und Informationswissenschaften spezialisiert hat. CiteSeerX bietet eine Vielzahl an Funktionen, angefangen beim automatischen Harvesten von wissenschaftlichen Publikationen im Web, über Textextraktion und Parsen von Publikationen und Referenzen bis hin zum automatischen Indexieren und Archivieren von Publikationen und deren Metadaten.

CiteSeerX verwendet einen sog. fokussierten Crawler, um wissenschaftliche Publikationen, in Form von PDF und Postscript Dokumenten, von Webseiten zu Harvesten. Aus diesen wird der Text extrahiert und geparst. Dies geschieht mit Hilfe von o.g. ParsCit. Aus dem geparsten Text werden die Dokument-Metadaten und die Referenzen extrahiert und strukturiert. Metadaten und die dazugehörigen Dokumente werden dann automatisch Indexiert und in einer Datenbank gespeichert. Das Web-Frontend dient als Suchmaschinen zur so aufgebauten digitalen Bibliothek.

CiteSeerX selbst bietet scheinbar keine API an. Obwohl es Publikationen bez. einer Citeseer API gibt \cite{Petinot:2004:CTS:1031171.1031275} ist diese nicht öffentlich erreichbar. Allerdings gibt es eine OAI PMH Schnittstelle \cite{citeseeroai}. Es ist prinzipiell möglich Anfragen an CiteSeerX mit Hilfe des Referenz-Managers Jabref zu stellen. Dieser Verwendet ebenfalls CiteSeerX, um Metadaten zu erfragen. Laut Erfahrungsberichten \cite{citeseerxapi} gibt es allerdings Probleme mit Such-Strings, die ":" enthalten. 

Alternativ kann mittels geeigneter Scriptsprachen (bspw. python) eine Anfrage gestellt und die Ergebnisse geparst werden. Hierfür sendet man einen speziell formatierten GET Request. Die Ergebnis-Seite kann dann geparsed werden.

\begin{lstlisting}[label=lst:citeseerx,caption=Suchanfragen an CiteSeerX.]
url = "http://citeseerx.ist.psu.edu/search?q="
q = "+" + string.replace(" ", "+") + "&submit=Search&sort=rlv&t=doc"
html = urlopen(url).read()
parsed = parse(html)
\end{lstlisting}

Interessant ist jedoch weniger die angebotene Suchmaschine, als vielmehr das Software-Projekt CiteSeerX selbst. Denn CiteSeerX ist komplett quelloffen und kann selbst gehostet werden. Somit kann eine Institution seine eigene CiteSeerX-Instanz aufbauen und automatisch eine digitale Bibliothek durch Harvesten der eigenen Seiten generieren. Allerdings hat CiteSeerX relativ hohe Hard- und Softwareanforderungen, da es sich um eine komplexe und Komplette Lösung mehrerer Komponenten, wie verschiedene Harvester, Datenbank-, Web- und Applikationsserver handelt. CiteSeerX-Instanzen könnten als Service für andere Organisationen gehostet werden.

CiteSeerX konzentriert sich allerdings auf das Harvesten von wissenschafltichen Puklikationen und die Extraktion von Metadaten aus den Publikationen selbst. Der Crawler von CiteSeerX kann nicht nach Publikationslisten auf Autorenseiten suchen. Eine Integration einer solchen Funktionalität mit o.g. Techniken ist allerdings denkbar.

\section{Related Work}

Es gibt im Bereich der Bibliographie-Verwaltung bereits einige Bemühungen unstrukturierte Bibliographie-Einträge automatisch zu strukturieren und zu deduplizieren.

\href{http://jabref.sourceforge.net/}{JabRef} ist ein Referenz-Manager speziell für Bibtex. Es bietet eine Schnittstelle zu Citeseer, über die es möglich ist, Felder der bibliographischen Einträge von Citeseer herunter zu laden. Hierfür muss man allderdings die CiteSeerURL des Eintrags im vornherein kennen. JabRef kann auch mehrere Bibtex-Dateien zusammenführen und berichtet über Duplikate.

\href{http://www.readcube.com/}{ReadCube} ist - ähnlich wie \href{http://www.mendeley.com/}{Mendeley} - ein Cloud-basierter Referenz-Manager. Er bietet Verbindungen zu mehreren Bibliographie-Datenbanken an (MAS, Google Scholar, CrossRef,PubMed, etc) um Publikationen zu Suchen, die Datenbank abzugleichen, automatisch beim Import von Publikationen Metadaten zu extrahieren, und mehr.

\href{http://www.mendeley.com/}{Mendeley} selbst bietet keine Schnittstelle zu Publikations-Datenbanken. Als Ersatz liefern sie allerdings ein Bookmarklet ``Save to Mendeley`` mit welchem auf Webseiten angezeigte Bibliographien direkt importiert werden können.

\href{http://fisica.cab.cnea.gov.ar/colisiones/staff/fiol/biblio-py.html}{biblio-py} ist ein in python geschriebener Bibliographie-Manager mit Such-Schnittstelle zur \href{Harvard Datanbank}{http://adsabs.harvard.edu/physics\_service.html}.

\href{biblio.webquery}{https://pypi.python.org/pypi/biblio.webquery/0.4.3b} und das darauf aufsetzende [librarian](https://github.com/EvansMike/librarian) erlauben Web-basierte Suche nach Büchern anhand von ISBN / ISSN.

\section{Bewertung und Ergebnis}

Betrachtet man lediglich die Extraktion von Publikations-Metadaten aus unstrukturierten webbasierten Referenzlisten, so bietet sich der Einsatz eines gewöhnlichen Harvesters und Integration einer einfachen Heuristik zum Erkennen von Publikationslisten an. Die Extraktion von Referenzkandidaten kann über einfache reguläre Ausdrücke realisiert werden. Das Parsen und strukturieren erfolgt über den Web Service oder eine eigene Instanz von FreeCite, welches die einfachste API besitzt und sehr gute Ergebnisse liefert.

Sollen zusätzlich Metadaten aus Publikationen selbst extrahiert werden und ggf. sogar automatisch eine digitale Bibliothek aufgebaut werden, so liefert CiteSeerX fast alles, was dazu benötigt wird. Der Crawler muss jedoch um die Funktionalität zum erkennen von Publikationslisten erweitert werden. Des weiteren muss die Extraktion von Referenzkandidaten integriert werden. Die Strukturierung kann mit Hilfe des mitgelieferten ParsCit erfolgen.

Andere Lösungen kommen auf Grund von Einschränkungen in Funktionalität, Nutzungsrichtlinien, oder der Komplexität nicht in Frage.
